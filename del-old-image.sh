#!/bin/bash

# Extract unique repository names
repos=$(docker images --format '{{.Repository}}' | sort -u)

# For each unique repo name, delete all but the latest tag
for repo in $repos; do
    # Skip specific images if you want
    # if [[ "$repo" == "nginx" || "$repo" == "ruby" ]]; then
    #     continue
    # fi

    # Fetch all tags of the repo sorted by creation time and leave out the latest
    old_tags=$(docker images --format '{{.Tag}}\t{{.CreatedSince}}' $repo | sort -k2 | head -n -1 | awk '{print $1}')

    # Remove the older tags
    for tag in $old_tags; do
        docker rmi $repo:$tag
    done
done

