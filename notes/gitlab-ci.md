---
title: gitlab-ci
tags: [Import-2038]
created: '2023-08-10T07:07:52.236Z'
modified: '2023-11-22T07:02:11.442Z'
---

---
title: 'Gitlab ci  教學'
date: '2023-08-11'
excerpt: 'gitlab,ci'
cover_image: '/images/posts/img1.jpg'
---


建立.gitlab-ci.yml


```
stages:
  - testing
  - build
  - pushlish
  - cleanup
  - deploy

run_test:
  only:
    - main
  tags:
    - vpn
  stage: testing
  script:
    - echo "run unit test"

```
說明
stages 
工作流

儲存後 
上傳到gitlab


參考
[為你自己學 GitLab CI/CD](https://www.youtube.com/watch?v=Fz_KjCvMdac)
[GitLab](https://gitlab.com/kaochenlong/shopping-cat-v2/-/blob/main/.gitlab-ci.yml?ref_type=heads)
