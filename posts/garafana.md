---
title: 'Garafana  教學'
date: '2023-11-13'
excerpt: 'Garafana,loki'
cover_image: '/images/posts/img1.jpg'
---



# 裝 grafana
https://computingforgeeks.com/how-to-install-grafana-on-debian-linux/?expand_article=1

# 使用Prometheus整合Grafana監控系統
https://www.youtube.com/watch?v=DIjF3Q7ch5U


# 安裝Loki 

sudo apt-get install -y software-properties-common
sudo add-apt-repository "deb https://packages.grafana.com/loki/deb stable main"

sudo apt-get install -y gnupg2
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -

sudo apt-get update

sudo apt-get install loki promtail

sudo systemctl start loki
sudo systemctl enable loki
sudo systemctl start promtail
sudo systemctl enable promtail

编辑 Promtail 配置文件：
sudo nano /etc/promtail/config.yml

####################################################
clients:
  - url: http://loki:3100/loki/api/v1/push

positions:
  filename: /var/log/app/app.log
  label: job
  timestamp: auto

  filename: /var/log/nginx/*.log
  label: job
  timestamp: auto

scrape_configs:
  - job_name: syslog
    static_configs:
      - targets:
          - localhost
        labels:
          job: varlogs
          __path__: /var/log/*log

####################################################


# 重启 Promtail 服务：

sudo systemctl restart promtail
