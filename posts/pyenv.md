---
title: 'Python切換版本'
date: '2023-11-20'
excerpt: 'python,pyenv'
cover_image: '/images/posts/img1.jpg'
---


```
pyenv install 3.8.12  # 你可以選擇安裝 Python 3.8 的特定小版本
```


```
pyenv global 3.8.12

```

查看版本
```
python --version
```


查看版本
```
pyenv version
```


使用 conda

```
conda create -n StreetFighterAI python=3.8.10
conda activate StreetFighterAI

```
