---
title: '开源web服务推荐'
date: '2023-11-18'
excerpt: '推荐'
cover_image: '/images/posts/img1.jpg'
---












```
容器管理： https://github.com/portainer/portainer
jupyter： https://jupyter-docker-stacks.readthe...
文档站： http://mrdoc.zmister.com/project-7/do...
vscode网页版： https://github.com/cdr/code-server
在线画图： https://github.com/jgraph/drawio
开源git： https://docs.gitea.io/zh-cn/install-w...
接口调试： https://github.com/hoppscotch/hoppscotch
爬虫调度框架： https://github.com/crawlab-team/crawlab
性能监控： https://hub.docker.com/r/netdata/netdata
可道云： https://kodcloud.com/download/
jetbrain全家桶： https://github.com/JetBrains/projecto...
镜像管理：https://hub.docker.com/_/registry
镜像管理web端： https://hub.docker.com/r/konradkleine...
网站导航： https://hub.docker.com/r/arvon2014/we...
为知笔记： https://www.wiz.cn/zh-cn/docker
nextcloud： https://nextcloud.com/install/#instru...
密码管理： https://github.com/bitwarden
webSSH工具： https://github.com/nirui/sshwifty
web数据库管理工具： https://github.com/OmniDB/OmniDB
mongodb管理工具：https://github.com/mrvautin/adminMongo
图床： https://github.com/chevereto/Cheveret...
下载工具： https://github.com/P3TERX/Aria2-Pro-D...
视频播放器：https://jellyfin.org/
电子书阅读： https://github.com/troyeguo/koodo-reader

```


[來源](https://www.youtube.com/watch?v=U0Ik7YLr6KE)
